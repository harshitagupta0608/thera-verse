from django.apps import AppConfig


class VrtConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vrt'
