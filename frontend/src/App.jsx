import {Routes, Route} from 'react-router-dom'
import Therapies from './pages/Therapies'
import HomePage from './pages/HomePage'

const App = () => {
  return (
    <Routes>
      <Route path='/' element={<HomePage />} /> 
      <Route path='/Therapies' element={<Therapies />} /> 

    </Routes>
  )
}

export default App